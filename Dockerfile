FROM debian:stable

# disable interactive functions
ENV DEBIAN_FRONTEND noninteractive

MAINTAINER Albatroz Jeremias <ajeremias@coletivos.org>

RUN apt -y update && apt -y upgrade

RUN apt -y install git

RUN git clone https://gitlab.com/edgyemma/Postmill.git /app

WORKDIR /app

## backend
RUN apt -y install apache2 composer curl gnupg nano

## PHP 7.2
RUN apt -y install ca-certificates apt-transport-https wget
RUN wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
RUN echo "deb https://packages.sury.org/php/ stretch main" | tee /etc/apt/sources.list.d/php.list
RUN apt update && echo "run update again"
RUN apt -y install php7.2 libapache2-mod-php7.2 php7.2-apcu php7.2-curl php7.2-pgsql php7.2-mbstring php7.2-gd php7.2-xml php7.2-zip php7.2-intl

RUN composer install

## frontend
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt -y install build-essential libssl-dev nodejs

RUN npm install
RUN npm run build-dev

RUN apt -y install postgresql postgresql-client

COPY ./.env.docker /app/.env

RUN rm -r /var/www/html/
RUN ln -s /app/public/ /var/www/html

ENV LOG errorlog

RUN a2enmod rewrite

CMD ["bin/console", "server:run", "0.0.0.0:8000"]
